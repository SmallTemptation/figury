package figury;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnimatorApp extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnAdd;
	private JButton btnAnimate;
	private AnimPanel kanwa;

	// Launch the application.
	
	class ResizeListener extends ComponentAdapter{			//skalowanie gui
		public void componentResized(ComponentEvent e) {
			kanwa.setBounds(10, 11, e.getComponent().getWidth() - 30, e.getComponent().getHeight() - 80);
			btnAdd.setBounds(10, e.getComponent().getHeight() - 65, 80, 23);
			btnAnimate.setBounds(100, e.getComponent().getHeight() - 65, 80, 23);
		}
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					frame.addComponentListener(frame.new ResizeListener());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension window = new Dimension(450, 300);
		Dimension kanwaSize = new Dimension(window.width-31, window.height-96);
		setBounds((screen.width-window.width)/2, (screen.height-window.height)/2, window.width, window.height);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);

		kanwa = new AnimPanel();
		
		setBackground(Color.WHITE);
		kanwa.setSize(kanwaSize);
		kanwa.setBounds(9, 11, kanwaSize.width, kanwaSize.height);
		kanwa.addComponentListener(kanwa.new PanelResizeListener());
		contentPane.add(kanwa);
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});

		btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.addFig();
			}
		});
		
		btnAdd.setBounds(10, 239, 80, 23);
		contentPane.add(btnAdd);
		
		btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kanwa.animate();
			}
		});
		
		btnAnimate.setBounds(100, 239, 80, 23);
		contentPane.add(btnAnimate);	
	}
}