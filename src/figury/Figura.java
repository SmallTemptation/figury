package figury;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.Random;

public abstract class Figura implements Runnable, ActionListener {
	
	protected Graphics2D buffer;// wspolny bufor
	protected Area area;
	protected Shape shape;// do wykreslania
	protected AffineTransform aft;// przeksztalcenie obiektu
	protected Dimension kanwaSize;
	
	private int dx, dy;// przesuniecie
	private double sf;// rozciaganie
	private double an;	// kat obrotu
	
	private int delay;
	private Color clr;
	private boolean pause;

	protected static final Random rand = new Random();

	public Figura(Graphics2D buf ,int del, int w, int h) {
		delay = del;
		buffer = buf;
		this.kanwaSize = new Dimension(w, h);
		
		dx = 1 + rand.nextInt(5);
		dy = 1 + rand.nextInt(5);
		sf = 1 + 0.05 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
	}
	
	public void notOnPause() {
		this.pause = false;
	}
	
	public void onPause() {
		this.pause = true;
	}
	
	@Override
	public void run() { // przesuniecie na srodek
		final Random rand = new Random();
		int startingPosition = rand.nextInt(180);
		aft.translate(startingPosition, startingPosition);
		area.transform(aft);
		shape = area;
		while(true) {
			shape = nextFrame();// przygotowanie nastepnego kwadru
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
			}
			while(pause) {
				try {	Thread.sleep(delay);
				} catch (InterruptedException e) {	
				}
			}
		}
	}

	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(area);
		aft = new AffineTransform();
		Rectangle bounds = area.getBounds();
		int cx = bounds.x + bounds.width / 2 ;
		int cy = bounds.y + bounds.height / 2;
		
		if(cx - bounds.height / 2 < 0) {// odbicie
			if(dx < 0) dx = -dx;
		}
		else if(cx + bounds.height / 2 > kanwaSize.width) {
			if(dx > 0) dx = -dx;
		}
		else if(cy - bounds.width / 2 < 0) {
			if(dy < 0) dy = -dy;
		}
		else if(cy + bounds.width / 2 > kanwaSize.height) {
			if(dy > 0) dy = -dy;
		}
		
		// zwiekszenie lub zmniejszenie
		if (bounds.height > kanwaSize.height / 3 || bounds.height < 10)
		sf = 1 / sf;
		// konstrukcja przeksztalcenia
		aft.translate(cx, cy);
		aft.scale(sf, sf);
		aft.rotate(an);
		aft.translate(-cx, -cy);
		aft.translate(dx, dy);
		// przeksztalcenie obiektu
		area.transform(aft);
		return area;
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		buffer.setColor(clr.brighter());
		buffer.fill(shape);
		// wykreslenie ramki
		buffer.setColor(clr.darker());
		buffer.draw(shape);
	}
	
	public void setKanwaSize(Dimension kanwaSize) {
		this.kanwaSize = kanwaSize;
	}
	
	public void setBuffer(Graphics2D buffer) {
		this.buffer = buffer;
	}

}