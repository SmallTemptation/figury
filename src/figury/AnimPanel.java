package figury;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	Image image; // bufor
	BufferedImage imageBufor;
	Graphics2D device; // wykreslacz ekranowy
	Graphics2D buffer; // wykreslacz bufora
	
	
	private int delay = 50;
	private Timer timer;
	private static int numer = 0;
	
	ArrayList <Figura> figury = new ArrayList<>();
	
	public AnimPanel() {
		super();
		setBackground(null);
		timer = new Timer(delay, this);
	}

	class PanelResizeListener extends ComponentAdapter{		//skalowanie animacji - resetuje ca�o��
		public void componentResized(ComponentEvent e) {
			image = createImage(e.getComponent().getWidth(), e.getComponent().getHeight()); // utworzenie nowego pustego kanwasa o dopasowanym rozmiarze
			buffer = (Graphics2D) image.getGraphics(); // utowrzenie nowego bufora na podstawie image
			for (Figura figura : figury) { // przepisanie wszystkich figur do nowego bufora
				figura.setKanwaSize(e.getComponent().getSize());
				figura.setBuffer(buffer);
	        }
			buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			device = (Graphics2D) getGraphics();
			device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}
	}
	
	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(width, height);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	}

	void addFig() {
		Figura fig = null;
		if(numer %3 == 0)	fig = new Kwadrat(buffer, delay, getWidth(), getHeight());
		if(numer %3 == 1)	fig = new Elipsa(buffer, delay, getWidth(), getHeight());
		if(numer %3 == 2)	fig = new Kolo(buffer, delay, getWidth(), getHeight());		
		figury.add(fig);
		timer.addActionListener(fig);
		new Thread(fig).start();
		numer++;
	}
	
	void animate() {
		if (timer.isRunning()) {
			timer.stop();
			for(Figura fig : figury)
				fig.onPause();
		} else {
			timer.start();
			for(Figura fig : figury)
				fig.notOnPause();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, getWidth(), getHeight());
	}
}